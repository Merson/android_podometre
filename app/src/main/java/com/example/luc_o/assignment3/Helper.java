package com.example.luc_o.assignment3;

import android.os.SystemClock;

/**
 * Created by luc-o on 25/04/2017.
 */

public class Helper {

    public static int getSeconds(long elapsedTime) {
        int hours = (int) (elapsedTime / 3600000);
        int minutes = (int) (elapsedTime - hours * 3600000) / 60000;
        int seconds = (int) (elapsedTime - hours * 3600000 - minutes * 60000) / 1000;
        return seconds + (minutes * 60) + hours * 3600;
    }

    public static int getHours(long elapsedTime) {
        int hours = (int) (elapsedTime / 3600000);
        return hours;
    }

    public static String getCorrectTime(int s) {
        /*if (s <= 60)
            return (s + "s");
        else if (s <= 3600)
            return (s / 60 + "min");
        else
            return (s / 3600 + "hours");*/
        return s + "s";
    }

    public static int getCorrectSize(int n) {
        if (n <= 5)
            return 80;
        else if (n <= 10)
            return 50;
        else if (n <= 15)
            return 20;
        else if (n <= 25)
            return 10;
        else return 5;
    }
}
