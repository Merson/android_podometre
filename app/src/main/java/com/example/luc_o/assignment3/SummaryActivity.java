package com.example.luc_o.assignment3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.DecimalFormat;

public class SummaryActivity extends AppCompatActivity {

    private DecimalFormat _df;

    private TextView _totalTimeSummaryTextView;
    private TextView _totalDistanceTravelledSummaryTextView;
    private TextView _averageSpeedSummaryTextView;

    private int _totalTime;
    private float _totalDistanceTravelled;
    private float _averageSpeed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        this._df = new DecimalFormat();
        this._df.setMaximumFractionDigits (2);

        this._totalTimeSummaryTextView = (TextView) findViewById(R.id.totalTimeSummaryTextView);
        this._totalDistanceTravelledSummaryTextView = (TextView) findViewById(R.id.totalDistanceTravelledSummaryTextView);
        this._averageSpeedSummaryTextView = (TextView) findViewById(R.id.averageSpeedSummaryTextView);

        this._totalTime = getIntent().getExtras().getInt("TOTAL_TIME");
        this._totalDistanceTravelled = getIntent().getExtras().getFloat("TOTAL_DISTANCE");
        this._averageSpeed = getIntent().getExtras().getFloat("AVERAGE_SPEED");

        this._totalTimeSummaryTextView.setText("Total time = " + Helper.getCorrectTime(this._totalTime));
        this._totalDistanceTravelledSummaryTextView.setText("Total distance travelled = " + this._df.format(this._totalDistanceTravelled) + "km");
        this._averageSpeedSummaryTextView.setText("Average speed = " + this._df.format(this._averageSpeed) + "km/h");
    }
}
