package com.example.luc_o.assignment3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by luc-o on 19/04/2017.
 */

public class SummaryCustomView extends View {

    public Context _context;
    private int _boardScreenSize;
    private int _boardDims;
    private int _cellSize;

    private Paint _paint;
    private Paint _paint2;
    private Paint _paint3;

    private int _km;
    private ArrayList<Integer> _historic;

    public SummaryCustomView(Context c, AttributeSet as) {
        super(c, as);
        init(c);
    }

    public SummaryCustomView(Context c, AttributeSet as, int default_style) {
        super(c, as, default_style);
        init(c);
    }

    private void init(Context c) {
        this._context = c;

        this._boardScreenSize = 0;
        this._boardDims = 10;
        this._cellSize = this._boardScreenSize / this._boardDims;

        this._paint = new Paint();
        this._paint.setColor(Color.BLACK);
        this._paint.setStrokeWidth(1);

        this._paint2 = new Paint();
        this._paint2.setColor(Color.RED);

        this._paint3 = new Paint();
        this._paint3.setColor(Color.BLACK);
        this._paint3.setTextSize(30);

        this._historic = MainActivity.getHistoric();
        this._km = this._historic.size();
        Toast.makeText(this.getContext(), "km = " + this._km, Toast.LENGTH_LONG).show();

    }

    @Override
    protected void onMeasure(int width, int height) {
        int parentWidth = MeasureSpec.getSize(width);
        int parentHeight = MeasureSpec.getSize(height);
        if(parentHeight > parentWidth)
            this._boardScreenSize = parentWidth;
        else
            this._boardScreenSize = parentHeight;
        this.setMeasuredDimension(this._boardScreenSize, this._boardScreenSize);
        this._cellSize = this._boardScreenSize / this._boardDims;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);


        int margin = (this.getWidth() / (this._km + 1));

        canvas.drawText("t=" + 10000 + "s", 100, 0, this._paint3);


        int scale = 0;

        for (int i = 0; i < this._km; ++i) {

            if (i > 0)
                scale = this._historic.get(i) - this._historic.get(i - 1);
            else
                scale = this._historic.get(i);

            if (scale != 0)
                canvas.drawRect(margin, this._boardScreenSize / scale * 3, margin + Helper.getCorrectSize(this._km), this._boardScreenSize/* / (this._historic.get(i) / 2)*/, this._paint2);

            canvas.drawText("" + scale + "s", margin, this._boardScreenSize - 40, this._paint3);

            System.out.println("CACA = " + this._boardScreenSize + " / " + "i = " + i + ": " + _historic.get(i) + " " + scale  + " = " + this._boardScreenSize / scale);
            margin += (this.getWidth() / (this._km + 1));
        }

        for(int i=0; i < this._boardDims; ++i) {
            canvas.drawLine(0, i * this._cellSize, this._boardScreenSize, i * this._cellSize, this._paint);
        }



    }
}
