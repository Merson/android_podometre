package com.example.luc_o.assignment3;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.SystemClock;
import android.provider.Settings;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class MainActivity extends Activity implements LocationListener {

    private LocationManager _lm;

    private float _currentSpeed;
    private float _totalDistanceTravelled;
    private float _averageSpeed;

    private int _dist;

    private float _totalSpeed;

    private int _points;

    private TextView _currentSpeedTextView;
    private TextView _totalDistanceTravelledTextView;
    private TextView _averageSpeedTextView;

    private Location _currentLocation;
    private Location _lastLocation;
    private Location _newLocation;

    private Chronometer _elapsedTimeChronometer;

    private Button _startButton;

    private Boolean _started;

    private static ArrayList<Integer> _historic;

    private DecimalFormat _df;

    private long _old = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this._lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        this._currentSpeed = 0.0f;
        this._totalDistanceTravelled = 0.0f;
        this._averageSpeed = 0.0f;

        this._dist = 1;

        this._totalSpeed = 0;

        this._points = 0;

        this._currentSpeedTextView = (TextView) findViewById(R.id.currentSpeedTextView);
        this._totalDistanceTravelledTextView = (TextView) findViewById(R.id.totalDistanceTravelledTextView);
        this._averageSpeedTextView = (TextView) findViewById(R.id.averageSpeedTextView);

        this._currentLocation = null;
        this._lastLocation = null;
        this._newLocation = null;

        this._elapsedTimeChronometer = (Chronometer) findViewById(R.id.elapsedTimeChronometer);
        this._elapsedTimeChronometer.setBase(SystemClock.elapsedRealtime());

        this._startButton = (Button) findViewById(R.id.startButton);

        this._started = false;

        _historic = new ArrayList<Integer>();

        this._df = new DecimalFormat();
        this._df.setMaximumFractionDigits (2);

        this._old = 0;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            this._lm.removeUpdates(this);
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if (this._currentLocation == null)
            this._currentLocation = location;
        if (this._currentLocation != null)
            this._newLocation = location;

        if (this._lastLocation != null)
            this._currentSpeed = this._lastLocation.distanceTo(this._newLocation) /* / Helper.getHours(SystemClock.elapsedRealtime()) * 10*/;
        else
            this._currentSpeed = location.getSpeed();


        if (this._lastLocation != null && this._newLocation != null)
            this._totalDistanceTravelled += this._lastLocation.distanceTo(this._newLocation) / 1000;

        this._totalSpeed += this._currentSpeed;
        if (this._points != 0)
            this._averageSpeed = this._totalSpeed / this._points;

        this._points += 1;

        this._currentSpeedTextView.setText("Current speed = " + this._df.format(this._currentSpeed) + "km/h");
        this._totalDistanceTravelledTextView.setText("Total distance travelled = " + this._df.format(this._totalDistanceTravelled) + "km");
        this._averageSpeedTextView.setText("Average speed = " + this._df.format(this._averageSpeed) + "km/h");

        if (this._totalDistanceTravelled >= this._dist) {
            _historic.add(Helper.getSeconds(SystemClock.elapsedRealtime() - this._elapsedTimeChronometer.getBase()));

            this._old = SystemClock.elapsedRealtime() - this._elapsedTimeChronometer.getBase();
            this._dist += 1;

        }

        this._lastLocation = location;
}

    @Override
    public void onProviderDisabled(String provider) {

        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
        Toast.makeText(getBaseContext(), "Gps is turned off!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(getBaseContext(), "Gps is turned on!! ",
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    public void onStartButton(View v) {
        if (!this._started) {
            refreshData();
            _historic.clear();
            this._elapsedTimeChronometer.setBase(SystemClock.elapsedRealtime());
            this._elapsedTimeChronometer.start();

            this._lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    2000, 1, this);
            Toast.makeText(getBaseContext(), "start recording",
                    Toast.LENGTH_SHORT).show();
            this._started = true;
            this._startButton.setText("Stop");
        } else {
            this._elapsedTimeChronometer.stop();
            Toast.makeText(getBaseContext(), "stop recording",
                    Toast.LENGTH_SHORT).show();
            this._started = false;
            this._startButton.setText("Start");
            this._lm.removeUpdates(this);

            Intent intent = new Intent(MainActivity.this, SummaryActivity.class);

            intent.putExtra("TOTAL_TIME", Helper.getSeconds(SystemClock.elapsedRealtime() - this._elapsedTimeChronometer.getBase()));
            intent.putExtra("TOTAL_DISTANCE", this._totalDistanceTravelled);
            intent.putExtra("AVERAGE_SPEED", this._averageSpeed);

            this.refreshData();

            startActivity(intent);
        }
    }

    private void refreshData() {
        this._currentSpeed = 0.0f;
        this._totalDistanceTravelled = 0.0f;
        this._averageSpeed = 0.0f;
        this._dist = 1;
        this._currentSpeedTextView.setText("Current speed = " + this._currentSpeed);
        this._totalDistanceTravelledTextView.setText("Total distance travelled = " + this._df.format(this._totalDistanceTravelled));
        this._averageSpeedTextView.setText("Average speed = " + this._df.format(this._averageSpeed));
        this._totalSpeed = 0;
        this._points = 0;
        this._currentLocation = null;
        this._newLocation = null;
        this._lastLocation = null;
    }

    public static ArrayList<Integer> getHistoric() {
        return _historic;
    }
}
